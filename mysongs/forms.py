from django import forms
from mysongs.models import register, Blog
from django.contrib.auth.models import User
class UserForm(forms.ModelForm):
    confirm_password = forms.CharField(widget=forms.PasswordInput())
    class Meta():
        model = User
        fields = ('first_name','username','email','password')
        widgets = {
            'first_name':forms.TextInput(attrs={'label':"Full Name"}),
            'password':forms.PasswordInput(attrs={'type':'password'}),
            'confirm_password':forms.PasswordInput(attrs={'type':'password'}),
        }
    def clean(self):
        data = super().clean()
        if data['password'] != data['confirm_password']:
            raise forms.ValidationError('Password Doed Not Match')

class registerForm(forms.ModelForm):    
    class Meta():
        model = register
        fields = ('profile_pic','city')
        widgets = {
            
        }

class blogForm(forms.ModelForm):
    class Meta():
        model = Blog
        fields = ('title','description','media')