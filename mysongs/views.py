from django.shortcuts import render
from mysongs.models import Blog,Song,song_type,languages,album, register, feedback
import random 
from mysongs.forms import UserForm,registerForm,blogForm
from django.http import HttpResponseRedirect
from django.contrib.auth import authenticate,login,logout
from django.contrib.auth.decorators import login_required

all_type = song_type.objects.all().order_by('title')
all_type_nine = song_type.objects.all().order_by('title')[:9]
all_lang = languages.objects.all().order_by('name')
all_songs = Song.objects.all()
blogs = Blog.objects.all().order_by('-id')
all_blogs = []
for i in blogs:
    dt = str(i.date).split('-')
    b = {}
    b['title'] = i.title[:100]
    b['desc'] = i.description[:150]
    b['date'] = dt[2]
    b['month'] = dt[1]
    b['year'] = dt[0]
    b['img'] = i.media
    all_blogs.append(b)
songs=[]
for i in range(4):
    n = random.randint(1,len(all_songs)+1)
    d = Song.objects.filter(id=n)
    if len(d)>=1:
        sng= Song.objects.get(pk=n)
        songs.append(sng)

def uslogin(request):
    if request.method == "POST":
        if 'un'in request.POST:
            usn = request.POST['un']
            pas = request.POST['pwd']
            user = authenticate(username= usn, password=pas)
            if user:
                    if user.is_active:
                            login(request,user)
                            response = HttpResponseRedirect('/')
                            if "remember" in request.POST:
                                    response.set_cookie('id',user.id)
                                    return response
                            else:
                                    return response
                    else:
                            status = "You are is not activated!!"
            else:
                    status ="You are not registred!!!!"
            return render(request,'index.html',{'all_type':all_type,'all_lang':all_lang,'all_type_nine':all_type_nine,'trending':songs,'status':status})
@login_required
def uslogout(request):
    logout(request)
    response = HttpResponseRedirect('/')
    response.delete_cookie('id')
    return response

def registerView(request):
    form1 = UserForm()
    form2 = registerForm()
    if request.method == "POST":
        form1 = UserForm(request.POST)
        form2 = registerForm(request.POST,request.FILES)
        if form1.is_valid() and form2.is_valid():
            user = form1.save()
            user.set_password(user.password)
            user.save()

            profile = form2.save(commit=False)
            profile.user = user
            if 'profile_pic' in request.FILES:
                pp = request.FILES['profile_pic']
                profile.profile_pic= pp
            profile.save()
            return render(request,'index.html',{'form1':form1,'form2':form2,'all_type':all_type,'all_lang':all_lang,'all_type_nine':all_type_nine,'status':'Account Created Successfully!!!'})
    
    return render(request,'signup.html',{'form1':form1,'form2':form2,'all_type':all_type,'all_lang':all_lang,'all_type_nine':all_type_nine,})

def index(request):
    print('all=',all_blogs)
    profile=[]
    data = Song.objects.all().order_by('-id')[:6]
    if request.user.is_authenticated:
        if not request.user.is_superuser:
            profile = register.objects.get(user__username=request.user.username)
    return render(request,'index.html',{'songs':data,'all_type':all_type,'all_lang':all_lang,'all_type_nine':all_type_nine,'trending':songs,'profile':profile,'all_blog':all_blogs})

def all_albums(request):
    if request.user.is_authenticated:
        if not request.user.is_superuser:
            profile = register.objects.get(user__username=request.user.username)
    albums = album.objects.all().order_by('title')
    return render(request,'charts.html',{'albums':albums,'trending':songs,'total':len(albums),'all_lang':all_lang,'all_type_nine':all_type_nine,'profile':profile})

def single_album(request):
    if 'id' in request.GET:
        id = request.GET['id']
        songs = Song.objects.filter(album__id=id).order_by('name')
        albums = album.objects.exclude(pk=id).order_by('title')
    return render(request,'single-charts.html',{'all_lang':all_lang,'all_type_nine':all_type_nine,'songs':songs,'other_albums':albums})

def filter_songs(request):
    all=[]
    for i in all_type:
        sng = {}
        sng['type']=i
        data = Song.objects.filter(type=i)
        sng['songs'] = data
        sng['total'] = len(data)
        all.append(sng)
 
    if 'name' in request.GET:
        name = request.GET['name']
        songs = Song.objects.filter(language__name=name).order_by('name')
        fil_lang = languages.objects.exclude(name=name).order_by('name')
        return render(request,'language-songs.html',{'all_lang':all_lang,'all_type_nine':all_type_nine,'songs':songs,'total':len(songs),'result':name,'fil_lang':fil_lang})
    if 'category' in request.GET:
        cat = request.GET['category']
        songs = Song.objects.filter(type__title=cat).order_by('name')
        fil_cat = song_type.objects.exclude(title=cat).order_by('title')
        return render(request,'language-songs.html',{'all_lang':all_lang,'all_type_nine':all_type_nine,'songs':songs,'total':len(songs),'result':cat,'fil_cat':fil_cat})

    if 'search' in request.GET:
        sr = request.GET['search']
        d = 1
        all = Song.objects.filter(type__title__contains=sr)|Song.objects.filter(name__contains=sr)|Song.objects.filter(album__title__contains=sr)|Song.objects.filter(singer__name__contains=sr)
        if len(all)==0:
            d = 0
        return render(request,'language-songs.html',{'all_lang':all_lang,'all_type_nine':all_type_nine,'all_results':all,'total_search':len(all),'sfor':sr,'d':d})    
    return render(request,'language-songs.html',{'all_lang':all_lang,'all_type_nine':all_type_nine,'all_songs':all})

def blogView(request):
    form = blogForm()
    profile = register.objects.get(user__username=request.user.username)
    if request.method == "POST":
        form = blogForm(request.POST,request.FILES)
        if form.is_valid():
            data = form.save(commit=False)
            data.user=profile
            data.save()
            return render(request,'post-blog.html',{'form':form,'profile':profile,'all_lang':all_lang,'all_type_nine':all_type_nine,'all_songs':all,'status':'Post Uploaded Successfully!!!','profile':profile})
    return render(request,'post-blog.html',{'form':form,'profile':profile,'all_lang':all_lang,'all_type_nine':all_type_nine,'all_songs':all,'all_blogs':all_blogs,'profile':profile})

def blog(request):
    if request.user.is_authenticated:
        if not request.user.is_superuser:
            profile = register.objects.get(user__username=request.user.username)
    if 'keyword' in request.GET:
        kw = request.GET['keyword']

        data = Blog.objects.filter(title__contains=kw)
        return render(request,'blog.html',{'blogs':data,'sr':len(data),'all_type':all_type,'all_lang':all_lang,'all_type_nine':all_type_nine,'profile':profile})
        
    return render(request,'blog.html',{'blogs':blogs,'all_type':all_type,'all_lang':all_lang,'all_type_nine':all_type_nine,'profile':profile})

def contact(request):
    if request.user.is_authenticated:
        if not request.user.is_superuser:
            profile = register.objects.get(user__username=request.user.username)
    if request.method=="POST":
        name = request.POST['name']
        email = request.POST['email']
        msz = request.POST['message']

        data = feedback(name=name,email=email,message=msz)
        data.save()
        return render(request,'contact.html',{'status':'Thanks for Your Feedback!!!','all_type':all_type,'all_lang':all_lang,'all_type_nine':all_type_nine,'profile':profile})
    return render(request,'contact.html',{'all_type':all_type,'all_lang':all_lang,'all_type_nine':all_type_nine,'profile':profile})