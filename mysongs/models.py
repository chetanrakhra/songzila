from django.db import models
from django.contrib.auth.models import User

class album(models.Model):
    title = models.CharField(max_length=200)
    added_on = models.DateField(auto_now_add=True)
    cover = models.ImageField(upload_to = "album_covers/%Y/%m/%d",null=True)

    def __str__(self):
        return self.title

class languages(models.Model):
    name = models.CharField(max_length=200)
    added_on = models.DateField(auto_now_add=True)

    def __str__(self):
        return self.name

class song_type(models.Model):
    title = models.CharField(max_length=1000)
    cover = models.ImageField(upload_to = "song_covers/%Y/%m/%d",null=True)
    added_on = models.DateField(auto_now_add=True)

    def __str__(self):
        return self.title

class singer(models.Model):
    name = models.CharField(max_length=200)
    Genres = models.CharField(max_length=200)
    Years_active=models.IntegerField(blank=True)
    website = models.URLField(blank=True)
    picture = models.ImageField(upload_to="singers/%Y/%m/%d")
    nationality = models.CharField(max_length=200,blank=True)
    added_on = models.DateField(auto_now_add=True)

    def __str__(self):
        return self.name

class Song(models.Model):
    name = models.CharField(max_length=1000)
    file = models.FileField(upload_to="songs/%Y/%m/%d")
    cover = models.ImageField(null=True,upload_to="song_cover/%Y/%m/%d",blank=True)
    lyrics = models.TextField(blank=True,null=True)
    singer = models.ForeignKey(singer,on_delete = models.CASCADE)
    type = models.ForeignKey(song_type,on_delete = models.CASCADE)
    language = models.ForeignKey(languages,on_delete = models.CASCADE)
    album = models.ForeignKey(album,on_delete = models.CASCADE)
    added_on_date = models.DateField(auto_now_add=True)
    added_on_time = models.TimeField(auto_now_add=True)
    status = models.BooleanField(default=True)
    clicks = models.IntegerField(default=0)

    def __str__(self):
        return self.name
    
class register(models.Model):
    user = models.OneToOneField(User,on_delete=models.CASCADE)
    profile_pic = models.ImageField(upload_to="profiles/%Y/%m/%d")
    city = models.CharField(max_length=200)
    added_on = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.user.username

class Blog(models.Model):
    user = models.ForeignKey(register,on_delete=models.CASCADE)
    title = models.CharField(max_length=500)
    description = models.TextField()
    date = models.DateField(auto_now_add=True)
    time = models.TimeField(auto_now_add=True)
    media = models.ImageField(upload_to="blog/%Y/%m/%d")

    def __str__(self):
        return self.user.user.username

class feedback(models.Model):
    name = models.CharField(max_length=250)
    email = models.EmailField(max_length=250)
    message = models.TextField()
    added_on = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self._check_long_column_names