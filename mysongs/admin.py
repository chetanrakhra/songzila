from django.contrib import admin
from mysongs.models import register,album,languages,song_type,singer,Song, Blog, feedback

admin.site.site_header="SongZila"
class albumAdmin(admin.ModelAdmin):
    list_display = ['title','added_on']

class languagesAdmin(admin.ModelAdmin):
    list_display = ['name','added_on']

class song_typeAdmin(admin.ModelAdmin):
    list_display = ['title','added_on']
    
class singerAdmin(admin.ModelAdmin):
    list_display = ['name','Genres','Years_active','website','nationality','added_on']

class SongAdmin(admin.ModelAdmin):
    list_display = ['name','singer','type','language','album','added_on_date','added_on_time','clicks']

class BlogAdmin(admin.ModelAdmin):
    list_display =['user','title','description','date','time']

class feedbackAdmin(admin.ModelAdmin):
    list_display =['name','email','message','added_on']

admin.site.register(register)
admin.site.register(feedback,feedbackAdmin)
admin.site.register(Blog,BlogAdmin)
admin.site.register(album,albumAdmin)
admin.site.register(languages,languagesAdmin)
admin.site.register(song_type,song_typeAdmin)
admin.site.register(singer,singerAdmin)
admin.site.register(Song,SongAdmin)

